/* START  - script to autoplay music and subtitle */
	document.getElementById("sound_track").addEventListener("play", typeWriter);
	
	var i = 0;
	var text = "GOTTA CATCH 'EM ALL";
	var speed = 50;

	function typeWriter()
	{
		if(i < text.length)
		{
			
			document.getElementById("flash_text").innerHTML += text.charAt(i);
			i++;
			setTimeout(typeWriter, speed);
		}
	}

/* END  - script to autoplay music and subtitle */