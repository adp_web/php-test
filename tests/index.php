<!doctype html>
<html>
	<head>
		<meta charset='utf-8'>
		<title>Pokemon</title>
		
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script src="../src/pokedexapp.js"></script>
		
		<style>
			.poke-container
			{
				display: flex;
				flex-wrap: wrap;
			}
		</style>
		
	</head>
	<body>
		<form action="">
			<p>Enter a comma seperated list of pokemon types. Ex: fire,ground</p>
			<input type="text" placeholder="Enter trainer pokemon types">
		</form>
		
		<main class="poke-container">
			
		</main>
		
		
		
		
		
	</body>
	
</html>