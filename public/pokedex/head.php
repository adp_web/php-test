<meta charset="utf-8">
<meta name="description" content="Pokemon Challenge">
<meta name="keywords" content="Pokemon,Pokedex,Pokemon Game,Pokemon Challenge">
<meta name="author" content="Alex Du Preez">
<!-- Controls how the browser will handle the pages dimensions and scaling 
	This instance it will follow the screen-width of the device that is being view from.
-->
<meta name="viewport" content="width=device-width, initial-scale=1">



<!-- GOOGLE FONTS FOR SITE -->
<link href="https://fonts.googleapis.com/css?family=Aldrich" rel="stylesheet">
<!-- SITE STYLES -->
<link rel="stylesheet" type="text/css" href="styles/pokedex_styles.css">
<title>POK&Eacute;MON - GOTTA CATCH &apos;EM ALL</title>