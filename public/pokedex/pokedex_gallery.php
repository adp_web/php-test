<table>
		<tr>
			<tr>
				<th colspan='12'>GENERAL STATS</th>
				<th colspan='13'>BATTLE STATS</th>
			</tr>
			<tr>
				<th rowspan='2'>ID</th>
				<th rowspan='2'>PHOTO</th>
				<th rowspan='2'>NAME</th>
				<th rowspan='2'>SPECIES</th>
				<th rowspan='2'>TYPE</th>
				<th rowspan='2'>FORMS</th>
				<th rowspan='2' colspan='2'>ABILITIES</th>
				<th rowspan='2' colspan='1'>MOVES</th>
				<th rowspan='2'>HEIGHT</th>
				<th rowspan='2'>WEIGHT</th>
				<th rowspan='2'>ORDER</th>
				
				<th rowspan='2'>NAME</th>
				<th rowspan='2'>STAT</th>
				<th rowspan='2'>EF.</th>
				
				<th colspan='2'>S.DEFENCE</th><th colspan='2'>S.ATTACK</th><th colspan='2'>DEFENCE</th><th colspan='2'>ATTACK</th><th colspan='2'>STAT NAME</th>
				<tr>
					<th>STAT</th><th>EF.</th>
					<th>STAT</th><th>EF.</th>
					<th>STAT</th><th>EF.</th>
					<th>STAT</th><th>EF.</th>
					<th>STAT</th><th>EF.</th>
				</tr>
			</tr>
		<tr>

<?php

/*
		+ PHP.INI FILE +
		INCREASE THE AMOUNT OF TIME ALOUD FOR A CONNECTION REQUEST TO BE OPEN
		INCREASE THE AMOUNT SIZE OF DATA TO BE REQUESTED

*/

/* pokemon_api link https://pokeapi.co/api/v2/ */
	
	$found = true;/* setting the variable found to true - this will let us know that there are items to be retrieved within the api */
	$id_start = 0;/* setting the variable id to 0 - Zero is the first bit of memory stored in a computer */
	$id_finish = 40;
	/*
		Using a while loop to pull all the records from the json array
		condition
		while the variable found is true and the id number is less than 15 execute code in braces
	*/
	while($found && $id_start < $id_finish)
	{
		/* file_get_contents - returns the file in a string data type */
		$data = file_get_contents("https://pokeapi.co/api/v2/pokemon/1" . $id_start);
		
		if($data != "")
		{
			$rData = json_decode($data, true);
			
			$pokemonID = $id_start;
			$pokemonIMG = $rData['sprites']['front_default'];
			$pokemonName = $rData['name'];
			$pokemonName->name;
			$pokemSpeciesName = $rData['species']['name'];
			$pokemonType = $rData['types']['0']['type']['name'];
			$pokemonForm = $rData['forms']['0']['name'];
			$pokemonAbil_one = $rData['abilities']['0']['ability']['name'];
			$pokemonAbil_two = $rData['abilities']['1']['ability']['name'];
			
			/*
				CHECKING IF THE POKEMON HAS A SECOND ABILITY
				IF THERE IS NO SECOND ABILITY THEN "NA" IS DISPLAYED
				
				ERROR Message is counting 3 errors
			*/
			if($pokemonAbil_two == null)
			{
				$pokemonAbil_two = "NA";
			}
			else
			{
				$pokemonAbil_two;
			}
			
			$pokemonMove = $rData['moves']['0']['move']['name'];
			$pokemonHeight = $rData['height'];
			$pokemonWeight = $rData['weight'];
			$pokemonOrder = $rData['order'];
			$pokemonStatName = $rData['stats']['0']['stat']['name'];
			$pokemonStatNumber = $rData['stats']['0']['base_stat'];
			$pokemonStatEffort = $rData['stats']['0']['effort'];
			$pokemonSupDefNum = $rData['stats']['1']['base_stat'];
			$pokemonSupDefEff = $rData['stats']['1']['effort'];
			$pokemonSupAttNum = $rData['stats']['2']['base_stat'];
			$pokemonSupAttEff = $rData['stats']['2']['effort'];
			$pokemonDefNum = $rData['stats']['3']['base_stat'];
			$pokemonDefEff = $rData['stats']['3']['effort'];
			$pokemonAttNum = $rData['stats']['4']['base_stat'];
			$pokemonAttEff = $rData['stats']['4']['effort'];
			$pokemonStatNum = $rData['stats']['5']['base_stat'];
			$pokemonStatEff = $rData['stats']['5']['effort'];
?>

	
				<!-- GENERAL SECTION -->
				<td><?php echo($pokemonID);?></td>
				<td><img class='pokemon_image' src="<?php echo($pokemonIMG); ?>"></td>
				<td><?php echo($pokemonName); ?></td>
				<td><?php echo($pokemSpeciesName); ?></td>
				<td><?php echo($pokemonType); ?></td>
				<td><?php echo($pokemonForm); ?></td>
				
				<!-- ABILITIES SECTION -->
				<td><?php echo($pokemonAbil_one); ?></td>
				
				
				<td><?php 
				
				//var_dump($pokemonAbil_2);
				
				echo($pokemonAbil_two); ?>
				
				
				</td>
				
				
				
				<td><?php echo($pokemonMove); ?></td>

				<!-- STATS SECTION -->
				<td><?php echo($pokemonHeight); ?></td>
				<td><?php echo($pokemonWeight); ?></td>
				<td><?php echo($pokemonOrder); ?></td>
				
				<td><?php echo($pokemonStatName); ?></td>
				<td><?php echo($pokemonStatNumber); ?></td>
				<td><?php echo($pokemonStatEffort); ?></td>
				
				<td><?php echo($pokemonSupDefNum); ?></td>
				<td><?php echo($pokemonSupDefEff); ?></td>
				
				<td><?php echo($pokemonSupAttNum); ?></td>
				<td><?php echo($pokemonSupAttEff); ?></td>
				
				<td><?php echo($pokemonDefNum); ?></td>
				<td><?php echo($pokemonDefEff); ?></td>
				
				<td><?php echo($pokemonAttNum); ?></td>
				<td><?php echo($pokemonAttEff); ?></td>
				
				<td><?php echo($pokemonStatNum); ?></td>
				<td><?php echo($pokemonStatEff); ?></td>
			</tr>
			
<?php
		}
		else
		{
			die("No pokemon found - scan finished ! ");
		}
		$id_start++;
	}
?>
			
		</tr>
	</table>			



