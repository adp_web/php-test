<div>
	<form method="POST" id='pokemon_search_form' name="pokemon_search" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<table id="pokemon_search_tbl">
			<tr>
				<th>
					POK&eACUTE;MON SEARCH
				</th>
			</tr>
			<tr>
				<td>
					<input type='text' name='pokemon_name' placeholder='e.g. Pikachu' maxlength='11' minlength='2' required >
				</td>
			</tr>
			<tr>
				<td>
					<button type="submit" name="pokemon_search_btn">CLICK TO SEARCH</button>
				</td>
			</tr>
		</table>
	</form>
</div>