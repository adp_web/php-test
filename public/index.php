<!DOCTYPE html>
	<html lang="en">
		<head>
			<?php include('head.php'); ?>
		</head>
		
		<body>
			<header class="header">
				<?php include('header.php'); ?>
			</header>
				
			<nav class="topnav">
				<?php include('nav.php'); ?>
			</nav>

			<section class="row">
				<!-- LEFT SIDE -->
				<aside class="column side">
					<?php include('left_column.php'); ?>
				</aside>

				<section class="column middle">
					<?php include('site_content.php'); ?>
				</section>
	  
				<aside class="column side">
					<?php include('right_column.php'); ?>
				</aside>
				
			</section>
			
			<footer class="footer">
				<?php include('footer.php'); ?>
			</footer>
		</body>
	</html>