<?php
/* pokemon_api link https://pokeapi.co/api/v2/ */


	
	$found = true;/* setting the variable found to true - this will let us know that there are items to be retrieved within the api */
	$id_start = 0;/* setting the variable id to 0 - Zero is the first bit of memory stored in a computer */
	$id_finish = 1;
	/*
		Using a while loop to pull all the records from the json array
		condition
		while the variable found is true and the id number is less than 15 execute code in braces
	*/
	while($found && $id_start < $id_finish)
	{
		/* file_get_contents - returns the file in a string data type */
		$data = file_get_contents("https://pokeapi.co/api/v2/pokemon/1" . $id_start);
		
		if($data != "")
		{
			$rData = json_decode($data, true);
			
			echo("
				<!-- INSERT IMAGE / SPRITES -->
				<img src=" . $rData['sprites']['back_default'] . ">
				<img src=" . $rData['sprites']['back_shiny'] . ">
				<img src=" . $rData['sprites']['front_default'] . ">
				<img src=" . $rData['sprites']['front_shiny'] . ">
				
				<table border='2'>
					<tr>
						<th colspan='5'>POKEDEX</th>
					</tr>
					<tr>
						<td>ID</td>
						<td>NAME</td>
						<td>SPECIES</td>
						<td>TYPE</td>
						<td>FORMS</td>
						
					</tr>
					<tr>
						<td>
							" . $id_start . "
						</td>
						<td>
							" . $rData['name'] . "
						</td>
						<td>
							" . $rData['species']['name'] . "
						</td>
						<td>
							" . $rData['types']['0']['type']['name'] . "
						</td>
						<td>
							" . $rData['forms']['0']['name'] . "
						</td>
					</tr>
				</table>
				<br/>
				<table border='2'><!-- ABILITIES TABLE -->
					<tr>
						<th colspan='2'>ABILITIES</th><th colspan='1'>MOVES</th>
					</tr>
					<tr>
						<td>
							" . $rData['abilities']['0']['ability']['name'] . "
						</td>
						<td>
							" . $rData['abilities']['1']['ability']['name'] . "
						</td>
						<td>
							" . $rData['moves']['0']['move']['name'] . "
						</td>
					</tr>
				</table>
				
				<br/>
				
				<table border='2'><!-- STATS TABLE -->
					<tr>
						<th colspan='5'>POKEMON STATISTICS</th>
					</tr>
					<tr>
						<td>HEIGHT</td>
						<td>WEIGHT</td>
						<td>ORDER</td>
					</tr>
					<tr>
						<td>
							" . $rData['height'] . "
						</td>
						<td>
							" . $rData['weight'] . "
						</td>
						<td>
							" . $rData['order'] . "
						</td>
					</tr>
					<tr>
						<td>STAT NAME</td>
						<td>BASE STAT</td>
						<td>EFFORT</td>
					</tr>
					<tr>
						<td>
							" . $rData['stats']['0']['stat']['name'] . "
						</td>
						<td>
							" . $rData['stats']['0']['base_stat'] . "
						</td>
						<td>
							" . $rData['stats']['0']['effort'] . "
						</td>
					</tr>
					<tr>
						<td>
							" . $rData['stats']['1']['stat']['name'] . "
						</td>
						<td>
							" . $rData['stats']['1']['base_stat'] . "
						</td>
						<td>
							" . $rData['stats']['1']['effort'] . "
						</td>
					</tr>
					<tr>
						<td>
							" . $rData['stats']['2']['stat']['name'] . "
						</td>
						<td>
							" . $rData['stats']['2']['base_stat'] . "
						</td>
						<td>
							" . $rData['stats']['2']['effort'] . "
						</td>
					</tr>
					<tr>
						<td>
							" . $rData['stats']['3']['stat']['name'] . "
						</td>
						<td>
							" . $rData['stats']['3']['base_stat'] . "
						</td>
						<td>
							" . $rData['stats']['3']['effort'] . "
						</td>
					</tr>
					<tr>
						<td>
							" . $rData['stats']['4']['stat']['name'] . "
						</td>
						<td>
							" . $rData['stats']['4']['base_stat'] . "
						</td>
						<td>
							" . $rData['stats']['4']['effort'] . "
						</td>
					</tr>
					<tr>
						<td>
							" . $rData['stats']['5']['stat']['name'] . "
						</td>
						<td>
							" . $rData['stats']['5']['base_stat'] . "
						</td>
						<td>
							" . $rData['stats']['5']['effort'] . "
						</td>
					</tr>
				</table>
			");
			
		}
		else
		{
			die("No pokemon found - scan finished ! ");
		}
		
		$id_start++;
		
	}
?>